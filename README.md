# genetic and PSO algorithm implementation

## Groupe:
*   Halladj Hamza
*   Tradd Wassim

## resourses used to complete the project:
1.  [Genetic Algorithms in Search, Optimization, and Machine Learning](https://books.google.dz/books/about/Genetic_Algorithms_in_Search_Optimizatio.html?id=3_RQAAAAMAAJ&source=kp_book_description&redir_esc=y)
Chapters 1 & 2 & 3

2.  [Engineering Optimization: Theory and Practice](https://books.google.dz/books/about/Engineering_Optimization.html?id=Q49YW0quqpIC&source=kp_book_description&redir_esc=y)
Chapter 13
3.  [Numpy documantation](https://numpy.org/doc/stable/reference/generated/numpy.argmin.html) Code samples and useful functions

